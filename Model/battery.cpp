#include <QTimer>
#include <QFile>
#include <QTextStream>

#include "battery.h"


Battery::Battery() :
    timer{ new QTimer(this) }
{
    connect(timer.data(), &QTimer::timeout, this, &Battery::checkBatteryLevel);
}


Battery::~Battery()
{
    stopTimer();
}


void Battery::startTimer()
{
    timer->start(TIMER_MILLISECONDS);
}


void Battery::stopTimer()
{
    timer->stop();
}


void Battery::checkBatteryLevel()
{
    QFile batteryFile(BATTERY_FILE_NAME);

    if (batteryFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        const int batteryValue = QTextStream(&batteryFile).readAll().toInt();

        emit batteryLevel(batteryValue);
    }
}
