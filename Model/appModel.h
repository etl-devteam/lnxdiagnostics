#ifndef APPMODEL_H
#define APPMODEL_H

#include <QTimer>
#include <VisionAR/Framework/abstractVisionARModel.h>
#include "battery.h"


class AppModel : public AbstractVisionARModel
{
    Q_OBJECT

public:
    AppModel();
    ~AppModel();

private:

    static constexpr char const * const BATTERY_FILE_NAME{ "/sys/devices/soc0/DinemaPowerBank/PercentBattery" };
    static constexpr char const * const SD_CARD_DIR      { "/dev/mmcblk1p1"                                   };
    static constexpr char const * const WIFI_FILE_NAME   { "/dev/mmcblk1p1"                                   };
    static constexpr char const * const ContrastFile     { "/dev/mmcblk1p1"                                   };





    enum : short {
        FIRST_WINDOW = 1
    };

    enum class AppStatus : short {WELCOME, CHOICE, INSTRUCTION};
    AppStatus status;

    enum class RadioOption : short {TIMER, MANUAL};
    RadioOption currOption;

    QTimer *timer;
    QScopedPointer<Battery> battery;

    void runModel() override;

    void backPressed() override {}
    void backReleased(int timePressed) override;

    void okPressed() override;
    void okReleased(int /*timePressed*/ = 0) override {}

    void forwardPressed() override;
    void forwardReleased(int /*timePressed*/) override {}

    void backwardPressed() override;
    void backwardReleased(int /*timePressed*/) override {}

    void singleTap() override {};
    void doubleTap() override {};

    void swipeForward() override  {};
    void swipeBackward() override {};

    void manageAlertsEnd(QString id, bool value) override;

signals:
    void batteryLevel(int percent);

public slots:

    void updateFromTimer();

};



#endif // APPMODEL_H
