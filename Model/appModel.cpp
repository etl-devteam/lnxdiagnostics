#include <QDebug>
#include <QDateTime>
#include "appModel.h"
#include "battery.h"
#include <QFile>
#include <QNetworkInterface>
#include <QCoreApplication>
#include <QProcess>
#include <QFileInfo>

AppModel::AppModel() :
    AbstractVisionARModel(),
    status(AppStatus::WELCOME),
    currOption (RadioOption::TIMER),
    timer(nullptr),
    battery(new Battery)
{
    connect(battery.data(), &Battery::batteryLevel, this, [this](int perc)
    {
       emit batteryLevel(perc);
    });
}

AppModel::~AppModel()
{
    if (timer != nullptr)
    {
        delete timer;
        timer = nullptr;
    }
}

void AppModel::runModel()
{
    battery->startTimer();
    emit showWindow(FIRST_WINDOW);
}

void AppModel::backReleased(int /*timePressed*/)
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit newAlert(1, "QuestionCloseApp", "Do you wish to return to the Launder?");
        break;

    case AppStatus::CHOICE:
        emit showWindow(FIRST_WINDOW);  // go back to welcome page
        status = AppStatus::WELCOME;
        break;

    case AppStatus::INSTRUCTION:
        if (currOption == RadioOption::TIMER && timer != nullptr)
        {
            timer->stop();  // not sure if need this
            delete timer;
        }
        break;

    default:
        qDebug() << "moriremo tutti\n";
        break;
    }
}

void AppModel::okPressed()
{
}


void AppModel::forwardPressed()
{
}


void AppModel::backwardPressed()
{    
}

void AppModel::manageAlertsEnd(QString id, bool value)
{
    if (status == AppStatus::WELCOME &&
            id == "QuestionCloseApp" &&
            value)
    {
        emit endApplication();
    }
}

void AppModel::updateFromTimer()
{
}
