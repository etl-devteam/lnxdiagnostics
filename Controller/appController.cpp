#include "appController.h"


AppController::AppController(AppView *view, AppModel *model, QString app_relative_path) :
    AbstractVisionARController(view, model, app_relative_path)
{
}


void AppController::connections()
{
connect((AppModel*)model, SIGNAL(batteryLevel(int)),     (AppView*)view, SLOT(batteryLevelUpdate(int)));
}


void AppController::endActions()
{
}
