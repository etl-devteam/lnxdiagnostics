#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <VisionAR/Framework/abstractVisionARController.h>

#include "Model/appModel.h"
#include "View/appView.h"


class AppController : public AbstractVisionARController
{
    Q_OBJECT

public:
    AppController(AppView *view, AppModel *model, QString app_relative_path);

private:
    void connections() override;
    void endActions()  override;
};


#endif // APPCONTROLLER_H
