#include "MainPage.h"
#include <QFileInfo>
#include <QDebug>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QDateTime>
#include <QDialog>
#include <QProcess>
#include <QHostInfo>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <QPixmap>
#include <iostream>
#include <ctime>
#include <QTimer>
#include <QTime>

#define IOCTL_ERGLASS_CMD_GETVERSION        0x81
#define IOCTL_ERGLASS_CMD_GETSTATUS         0x90
#define IOCTL_ERGLASS_CMD_GETERRORS         0x91
#define INPUT_GLS           "/dev/usb/eRGlass0"


typedef struct {
    unsigned char protocol_major;
    unsigned char protocol_minor;
    unsigned char boot_major;
    unsigned char boot_minor;
    unsigned char appl_major;
    unsigned char appl_minor;
    unsigned char appl_patch;
    unsigned char appl_day;
    unsigned char appl_month;
    unsigned char appl_year;
    unsigned char hw_revision;
} __attribute__((packed)) ERGLASSVERSION;

const uint TIMEOUT_SHELL_START  = 10000;
const uint TIMEOUT_SHELL_FINISH = 10000;


//Check SD card, i check if the mount point exists
bool CheckSdCard()
{
    const QFileInfo outputDir("/dev/mmcblk1");
    if ((!outputDir.exists()) || (!outputDir.isDir())) {
        qWarning() << "directory does not exist";
        return false;
    }
    return true;
}

//Check and show the ip address it is not used currently, but it works
QString showIpAddress(){
    QString ipaddr;
    const QHostAddress &localhost = QHostAddress(QHostAddress::LocalHost);
    for (const QHostAddress &address: QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != localhost)
            qDebug() << address.toString();
        ipaddr = address.toString();
        return ipaddr;
    }
    return "";
}

//function to get the local date // NOT USED CURRENTLY BUT IT WORKS
QString getTime(){
    QDateTime this_time= QDateTime::currentDateTime();
    QString this_time_string= this_time.toString("dd MMMM yyyy");
    return this_time_string;
    //Pay attention if use this one, it may take the time of the CU, it is the default one (1 Jan 1970)
}//

//function to get the value of contrast
QString getContrast(){
    QString line;
    QFile file("/sys/class/graphics/fb0/contrast");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";
    
    QTextStream in(&file);
    while (!in.atEnd()) {
        return line = in.readLine();
    }
    return "";
}

//function to get the value of the brightness
QString getBrightness(){
    QString line;
    QFile file("/sys/class/graphics/fb0/brightness");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";
    QTextStream in(&file);
    while (!in.atEnd()) {
        return line = in.readLine();
    }
    return "";
}
//function to get the value of the CDK version
QString getCdkVersion(){
    QString line;
    QFile file("/var/Cdk_Version/version.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";
    
    QTextStream in(&file);
    while (!in.atEnd()) {
        return line = in.readLine();
    }
    return "";
}
//function to get the value of the Firmware version
QString getVersionFirmware(){
    int fd;
    int result;
    ERGLASSVERSION ver;
    QString finalString;
    
    if ((fd = open(INPUT_GLS,O_RDONLY)) < 0) return "";
    
    if ((result = ioctl(fd, IOCTL_ERGLASS_CMD_GETVERSION, &ver)) == -1) {
        close(fd);
        
        return "";
    }
    close(fd);
    finalString="Firmware Version: " + QString::number((int)ver.appl_major) + "." + QString::number((int)ver.appl_minor) + "." + QString::number((int)ver.appl_patch);
    return finalString;
}

//Function that checks if wifi is running and if it is working it returns the ip address
bool getIpAddress_Wifi(){
    for(const QNetworkInterface& iface : QNetworkInterface::allInterfaces()) {
        if (iface.type() == QNetworkInterface::Wifi && iface.flags().testFlag(QNetworkInterface::IsUp)) {
            qDebug() << iface.humanReadableName() << "(" << iface.name() << ")"
                     << "is up:" << iface.flags().testFlag(QNetworkInterface::IsUp)
                     << "is running:" << iface.flags().testFlag(QNetworkInterface::IsRunning);
            return true;
        }
        return false;
    }
    return false;
}
//Function to get the ip address of all active interfaces
QString giveIP(){
    QString localhostname =  QHostInfo::localHostName();
    QString localhostIP;
    QList<QHostAddress> hostList = QHostInfo::fromName(localhostname).addresses();
    foreach (const QHostAddress& address, hostList) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address.isLoopback() == false) {
            localhostIP = address.toString();
            return localhostIP;
        }
    }
    QString localMacAddress;
    QString localNetmask;
    foreach (const QNetworkInterface& networkInterface, QNetworkInterface::allInterfaces()) {
        foreach (const QNetworkAddressEntry& entry, networkInterface.addressEntries()) {
            if (entry.ip().toString() == localhostIP) {
                localMacAddress = networkInterface.hardwareAddress();
                localNetmask = entry.netmask().toString();
                break;
            }
        }
    }
    qDebug() << "Localhost name: " << localhostname;
    qDebug() << "IP = " << localhostIP;
    qDebug() << "MAC = " << localMacAddress;
    qDebug() << "Netmask = " << localNetmask;
    return "IP: " + localhostIP;
}

//Function to get the status of the CU and to convert it into exadecimal
QString getStatus(const char *filename)
{
    int status; //short
    int fd;
    int result;
    if ((fd = open((const char *)filename,O_RDONLY)) < 0) return "";
    
    if ((result = ioctl(fd, IOCTL_ERGLASS_CMD_GETSTATUS, &status)) != 0) { //getErrors in esadecimale
        close(fd);
        return "";
    }
    close(fd);
    QString risultato = "0x" +QString::number(status,16);
    qDebug() << "status: " << status;
    return ("Status: " + risultato);
}
//Function to get the errors of the CU and to convert it into exadecimal
QString getErrors(const char *filename)
{
    short status; //short
    int fd;
    int result;
    if ((fd = open((const char *)filename,O_RDONLY)) < 0) return "";
    
    if ((result = ioctl(fd, IOCTL_ERGLASS_CMD_GETERRORS, &status)) != 0) { //getErrors in esadecimale
        close(fd);
        return "";
    }
    close(fd);
    QString risultato ="0x" +QString::number(status, 16);
    qDebug() << "errors: " << status;
    return ("Errors: " + risultato);
}

//Function to update the battery level
void MainPage::batteryLevelUpdate(int percent){
    mainPage->Battery_label->setText(QString::number(percent));
}

//function to open a shell for commands
QString execShellCommand(const QString &command)
{
    QString resp;
    QProcess shell;
    shell.start("/bin/sh", QStringList() << "-c" << command);
    // check for starting successfully, default time is 30 seconds
    if (!shell.waitForStarted(TIMEOUT_SHELL_START)) {
        resp = "not started";
    } // check for timeout, default is 30 seconds
    else if (!shell.waitForFinished(TIMEOUT_SHELL_FINISH)) {
        resp = "timeout";
    } // read both output and error channels
    else {
        resp = shell.readAll();
    }
    // close the process
    shell.close();
    return resp;
}
//function to check if the wifi is activated or not
bool checkWIFI(){
    QString cmd  = "cat /sys/class/net/wlan0/carrier";    // for releasing all rfcomm
    QString resp = execShellCommand(cmd);
    if(resp=="0"){
        return false;
    }
    else
        return true;
}
//function to check if the eth0 interface is active or not
bool checkETH(){
    QString cmd  = "cat /sys/class/net/eth0/carrier";    // for releasing all rfcomm
    QString resp = execShellCommand(cmd);
    if(resp=="0"){
        return false;
    }
    else
        return true;
}
//function to connect to a ntpd server and show the date
QString check_realDate(){
    if (!(checkETH() && checkWIFI())){
        
    }
    else{
        QString cmd  = "ntpd -p 91.187.220.26 &";    // for releasing all rfcomm
        QString resp = execShellCommand(cmd);
        QString cmd2  = "date +\"%d-%m-%y\"";    // for releasing all rfcomm
        QString resp2 = execShellCommand(cmd2);
        return resp2;
    }
    return "";
}
//function to connect to a ntpd server and show the time l
QString check_realTime(){
    if(!checkETH() && !checkWIFI()){
        
    }
    else{
        QString cmd  = "ntpd -p 91.187.220.26 &";    // for releasing all rfcomm
        QString resp = execShellCommand(cmd);
        QString cmd2  = "date +\"%T\"";    // for releasing all rfcomm
        QString resp2 = execShellCommand(cmd2);
        return resp2;
    }
    return "";
}

void MainPage::UpdateTime()
{
    mainPage->Time_label->setText(QTime::currentTime().toString("hh:mm:ss"));
}



MainPage::MainPage(QObject *parent) : QObject(parent),
    mainPage(new Ui_MainPage),
    timer_1s(new QTimer(this))
{
    QObject::connect(timer_1s.data(), &QTimer::timeout, this, &MainPage::UpdateTime);
    timer_1s->start(100);
}

MainPage::~MainPage()
{
    delete mainPage;
    mainPage = nullptr;
}

void MainPage::setupUi(QWidget *widget)
{
    const QPixmap WIFI_PIX(":/5X1/5X1/wifi.png");
    const QPixmap NO_WIFI_PIX(":/5X1/5X1/no_wifi.png");
    
    mainPage->setupUi(widget);
    if (!CheckSdCard())
        mainPage->SdCard_label->hide();
    mainPage->Brightness_label->setText(getBrightness());
    mainPage->Contrast_label->setText(getContrast());
    if(!(checkETH() && checkWIFI())){
        mainPage->DayTime_label->hide();
        mainPage->Date_Label->hide();
        mainPage->Time_label_2->hide();
        mainPage->Time_label->hide();
    }
    else{
        mainPage->DayTime_label->setText(check_realDate());
    }
    mainPage->IpAddress_label->setText("Ip Addr: " + giveIP());
    mainPage->VersioneCDK_label->setText("CDK Version: " + getCdkVersion());    
    mainPage->brightness_image_label->show();
    mainPage->Contrast_image_label->show();
    mainPage->VersionFirmware_labe->setText(getVersionFirmware());
    mainPage->Status_label->setText(getStatus(INPUT_GLS));
    mainPage->Errors_label->setText(getErrors(INPUT_GLS));
    mainPage->Time_label->setText(check_realTime());
    mainPage->Info_label->show();
    
    

    
    if(getIpAddress_Wifi()){
        mainPage->Wifi_label->setPixmap(WIFI_PIX.scaled(mainPage->Wifi_label->size(),    Qt::KeepAspectRatio, Qt::SmoothTransformation));
    } 
    else {
        mainPage->Wifi_label->setPixmap(NO_WIFI_PIX.scaled(mainPage->Wifi_label->size(),    Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    mainPage->Wifi_label->show();
}
