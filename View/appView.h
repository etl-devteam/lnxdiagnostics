#ifndef APPVIEW_H
#define APPVIEW_H

#include <VisionAR/Framework/abstractVisionARView.h>
#include <QScopedPointer>
#include "MainPage.h"

class AppView : public AbstractVisionARView
{
    Q_OBJECT

public:
    AppView();

private:
    QScopedPointer<MainPage> mainpage;

public slots:
   void batteryLevelUpdate(int);

signals:
};


#endif // APPVIEW_H
