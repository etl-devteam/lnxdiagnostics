#include "appView.h"



AppView::AppView() :
    AbstractVisionARView(),
    mainpage(new MainPage)
{
    registerWindow(mainpage.data());
    // the order in which you "register window"s here
    // matches the number you pass in "showWindow(int)"
}

void AppView::batteryLevelUpdate(int percent){
    mainpage->batteryLevelUpdate(percent);
}
