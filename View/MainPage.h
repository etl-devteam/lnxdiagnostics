#ifndef WELCOMEPAGE_H
#define WELCOMEPAGE_H

#include "ui_MainPage.h"
#include <QObject>


class MainPage : public QObject
{
    Q_OBJECT
public:

    explicit MainPage(QObject *parent = nullptr);
    virtual ~MainPage();

    void setupUi(QWidget *widget);
    void batteryLevelUpdate(int);

private:
    Ui_MainPage *mainPage;
    QScopedPointer<QTimer> timer_1s;

    QString execShellCommand(const QString &command);

private slots:
    void UpdateTime();

};




#endif // WELCOMEPAGE_H
